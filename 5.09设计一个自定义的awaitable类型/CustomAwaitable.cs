﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._09设计一个自定义的awaitable类型
{
    class CustomAwaitable
    {
        private readonly bool completeSynchronously;

        public CustomAwaitable(bool completeSynchronously)
        {
            this.completeSynchronously = completeSynchronously;
        }

        public CustomAwaiter GetAwaiter()
        {
            return new CustomAwaiter(completeSynchronously);
        }
    }
}
