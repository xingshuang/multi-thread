﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._09设计一个自定义的awaitable类型
{
    class Program
    {
        static void Main(string[] args)
        {
            Task t = AsynchronousProcessing();
            t.Wait();
            Console.ReadLine();
        }

        private async static Task AsynchronousProcessing()
        {
            CustomAwaitable sync = new CustomAwaitable(true);
            string result = await sync;
            Console.WriteLine(result);

            CustomAwaitable async1 = new CustomAwaitable(false);
            result = await async1;
            Console.WriteLine(result);
        }
    }
}
