﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _1._7线程优先级
{
    class ThreadSample
    {
        private bool isStopped = false;

        public void Stop()
        {
            this.isStopped = true;
        }

        public void CountNumbers()
        {
            long counter = 0;
            while (!this.isStopped)
            {
                counter++;
            }
            Console.WriteLine($"{Thread.CurrentThread.Name} with {Thread.CurrentThread.Priority} priority has a count = {counter.ToString("N0")}");
        }
    }
}
