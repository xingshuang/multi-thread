﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _3._06在线程池中使用等待事件处理器及超时
{
    class Program
    {
        static void Main(string[] args)
        {
            RunOperation(TimeSpan.FromSeconds(5));
            RunOperation(TimeSpan.FromSeconds(7));
            Console.ReadLine();
        }

        static void RunOperation(TimeSpan workerOperationTimeout)
        {
            using (var evt = new ManualResetEvent(false))
            {
                using (var cts = new CancellationTokenSource())
                {
                    Console.WriteLine("Registering timeout operations...");
                    var worker = ThreadPool.RegisterWaitForSingleObject(evt, (state, isTimeout) => WorkerOperationWait(cts, isTimeout), null, workerOperationTimeout, true);
                    Console.WriteLine("Starting long running operation...");
                    ThreadPool.QueueUserWorkItem(_ => WorkerOperation(cts.Token, evt));
                    Thread.Sleep(workerOperationTimeout.Add(TimeSpan.FromSeconds(2)));
                    worker.Unregister(evt);
                }
            }
        }

        private static void WorkerOperation(CancellationToken token, ManualResetEvent evt)
        {
            for (int i = 0; i < 6; i++)
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            evt.Set();
        }

        private static void WorkerOperationWait(CancellationTokenSource cts, bool isTimeout)
        {
            if (isTimeout)
            {
                cts.Cancel();
                Console.WriteLine("Worker operation timed out and was canceled");

            }
            else
            {
                Console.WriteLine("Worker operation succeded.");
            }
        }
    }
}
