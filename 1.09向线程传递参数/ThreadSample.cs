﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _1._9向线程传递参数
{
    class ThreadSample
    {
        private readonly int iterations;

        public ThreadSample(int iterations)
        {
            this.iterations = iterations;
        }

        public void CountNumbers()
        {
            for (int i = 0; i < this.iterations; i++)
            {
                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                Console.WriteLine($"{Thread.CurrentThread.Name} prints {i}");
            }
        }
    }
}
