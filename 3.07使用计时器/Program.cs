﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _3._07使用计时器
{
    class Program
    {
        static Timer timer;

        static void Main(string[] args)
        {
            Console.WriteLine("Press 'Enter' to stop the timer");
            DateTime start = DateTime.Now;
            timer = new Timer(_ => TimerOperation(start), null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(2));

            Thread.Sleep(TimeSpan.FromSeconds(6));
            timer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(4));
            Console.ReadLine();
            timer.Dispose();
            Console.ReadLine();

        }

        private static void TimerOperation(DateTime start)
        {
            TimeSpan elapsed = DateTime.Now - start;
            Console.WriteLine($"{elapsed.Seconds} seconds from {start}. Timer thread pool thread id: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}
