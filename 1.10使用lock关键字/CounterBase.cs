﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._10使用lock关键字
{
    abstract class CounterBase
    {
        public abstract void Increment();
        public abstract void Decrement();
    }
}
