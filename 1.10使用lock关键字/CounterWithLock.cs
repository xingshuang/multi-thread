﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1._10使用lock关键字
{
    class CounterWithLock : CounterBase
    {
        private readonly object syncRoot = new object();

        public int Count { get; private set; }

        public override void Decrement()
        {
            lock (this.syncRoot)
            {
                Count--;
            }
        }

        public override void Increment()
        {
            lock (this.syncRoot)
            {
                Count++;
            }
        }
    }
}
