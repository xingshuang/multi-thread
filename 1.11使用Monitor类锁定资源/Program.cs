﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _1._11使用Monitor类锁定资源
{
    class Program
    {
        static void Main(string[] args)
        {
            object lock1 = new object();
            object lock2 = new object();
            new Thread(() => LockTooMuch(lock1, lock2)).Start();

            lock (lock2)
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Console.WriteLine("Monitor.TryEnter allows not to get stuck, returning false after a specified timeout is elapsed");
                if (Monitor.TryEnter(lock1, TimeSpan.FromSeconds(5)))
                {
                    Console.WriteLine("Acquired a protected resource successfully");
                }
                else
                {
                    Console.WriteLine("Timeout acquiring a resource");
                }
            }

            Console.WriteLine("------------------------------------");

            new Thread(() => LockTooMuch(lock1, lock2)).Start();
            lock (lock2)
            {
                Console.WriteLine("This will be a dealock!");
                Thread.Sleep(TimeSpan.FromSeconds(1));
                lock (lock1)
                {
                    Console.WriteLine("Acquired a protected resource successfully");
                }
            }
        }

        static void LockTooMuch(object lock1, object lock2)
        {
            lock (lock1)
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                lock (lock2) { }
            }
        }
    }
}
