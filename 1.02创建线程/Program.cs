﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _1._2创建线程
{
    class Program
    {
        static void Main(string[] args)
        {
            // 线程创建
            Thread t = new Thread(PrintNumbers);
            t.Start();
            PrintNumbers();
            Console.ReadLine();

        }

        static void PrintNumbers()
        {
            Console.WriteLine("PrintNumbers Starting...");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        
    }
}
