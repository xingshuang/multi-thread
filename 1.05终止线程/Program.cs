﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _1._5终止线程
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting program...");
            Thread t = new Thread(PrintNumbersWithDelay);
            t.Start();
            Thread.Sleep(TimeSpan.FromSeconds(6));
            // 线程终止
            t.Abort();
            Console.WriteLine("A thread has been aborted");
            Thread s = new Thread(PrintNumbers);
            s.Start();
            PrintNumbers();
            Console.ReadLine();
        }

        static void PrintNumbersWithDelay()
        {
            Console.WriteLine("Starting PrintNumbersWithDelay...");
            for (int i = 0; i < 10; i++)
            {
                // 线程暂停
                Thread.Sleep(TimeSpan.FromSeconds(2));
                Console.WriteLine(i);
            }
        }

        static void PrintNumbers()
        {
            Console.WriteLine("Starting PrintNumbers...");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
